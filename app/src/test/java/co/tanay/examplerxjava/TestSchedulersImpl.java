package co.tanay.examplerxjava;

import co.tanay.examplerxjava.rx.AppSchedulers;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class TestSchedulersImpl extends AppSchedulers {

    @Override
    public Scheduler io() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler ui() {
        return Schedulers.trampoline();
    }
}
