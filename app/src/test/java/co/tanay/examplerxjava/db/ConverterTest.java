package co.tanay.examplerxjava.db;

import org.junit.Test;
import org.threeten.bp.Instant;

public class ConverterTest {

    @Test
    public void assertCorrectnessInstantToLong() {
        Instant now = Instant.now();
        assert Converters.toTimeStamp(now) == now.toEpochMilli();
    }

    @Test
    public void assertCorrectnessLongToInstant() {
        Instant now = Instant.now();
        assert Converters.fromTimeStamp(now.toEpochMilli()).equals(now);
    }
}
