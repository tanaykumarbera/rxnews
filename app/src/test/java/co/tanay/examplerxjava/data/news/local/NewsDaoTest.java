package co.tanay.examplerxjava.data.news.local;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.threeten.bp.Instant;

import java.util.ArrayList;
import java.util.List;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import co.tanay.examplerxjava.db.NewsDatabase;

@RunWith(RobolectricTestRunner.class)
public class NewsDaoTest {

    NewsDao newsDao;

    NewsDatabase database;

    @Before
    public void setup() {
        database = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(),
            NewsDatabase.class
        ).allowMainThreadQueries()
            .build();
        newsDao = database.newsDao();
    }

    @After
    public void cleanup() {
        database.close();
    }

    private NewsArticle newArticle(String bla) {
        NewsArticle article = new NewsArticle();
        article.title = "test" + bla;
        article.imageUrl = "http://image.il/" + bla;
        article.contentUrl = "http://content.il" + bla;
        article.publishedBy = "author";
        article.description = "desc";
        article.publishedAt = Instant.now();
        return article;
    }

    @Test
    public void assertStore() {

        List<NewsArticle> news = new ArrayList<>();
        news.add(newArticle("1"));
        news.add(newArticle("2"));
        news.add(newArticle("3"));

        newsDao.addAll(news)
            .test()
            .assertNoErrors()
            .assertComplete();
    }

    @Test
    public void assertFetch() {

        List<NewsArticle> news = new ArrayList<>();
        news.add(newArticle("1"));
        news.add(newArticle("2"));
        news.add(newArticle("3"));

        newsDao.addAll(news).subscribe();

        newsDao.all()
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValue(value -> value.size() == 3);
    }

    @Test
    public void assertGet() {

        List<NewsArticle> news = new ArrayList<>();
        news.add(newArticle("1"));
        news.add(newArticle("2"));
        news.add(newArticle("3"));

        newsDao.addAll(news).subscribe();

        NewsArticle identical = newArticle("2");

        newsDao.get(identical.contentUrl)
            .test()
            .assertNoErrors()
            .assertComplete();
    }

    @Test
    public void assertClear() {

        List<NewsArticle> news = new ArrayList<>();
        news.add(newArticle("1"));
        news.add(newArticle("2"));
        news.add(newArticle("3"));

        newsDao.addAll(news).subscribe();

        newsDao.clear().subscribe();

        newsDao.all()
            .test()
            .assertNoErrors()
            .assertValue(List::isEmpty)
            .assertComplete();
    }
}
