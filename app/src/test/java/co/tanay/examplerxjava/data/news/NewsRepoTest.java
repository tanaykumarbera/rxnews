package co.tanay.examplerxjava.data.news;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.tanay.examplerxjava.data.news.local.NewsDao;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import co.tanay.examplerxjava.data.news.net.NewsApi;
import co.tanay.examplerxjava.data.news.net.model.NewsResponse;
import co.tanay.examplerxjava.data.news.net.model.NewsResponseItem;
import co.tanay.examplerxjava.utils.NetworkError;
import co.tanay.examplerxjava.utils.Resource;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NewsRepoTest {

    @Mock
    NewsApi mockNewsApi;

    @Mock
    NewsDao mockNewsDao;

    @Mock
    List<NewsResponseItem> mockNetworkResponse;

    @Mock
    List<NewsArticle> mockLocalResponse;

    NewsRepo newsRepo;

    private NewsResponseItem getItem() {
        NewsResponseItem item = new NewsResponseItem();
        item.title = "Test";
        item.author = "test";
        item.publishedAt = new Date();
        item.url = "https://test.io";
        item.urlToImage = "https://test.io";
        item.description = "tester";
        return item;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        newsRepo = new NewsRepoImpl(mockNewsApi, mockNewsDao);
    }

    @Test
    public void assertSuccessfulNetworkFetch() {

        List<NewsResponseItem> list = new ArrayList<>();
        list.add(getItem());
        list.add(getItem());
        NewsResponse response = new NewsResponse();
        response.articles = list;

        when(mockNewsDao.all()).thenReturn(Maybe.empty()).thenReturn(Maybe.just(mockLocalResponse));
        when(mockNewsDao.addAll(any())).thenReturn(Completable.complete());
        when(mockNewsApi.fetch(anyString())).thenReturn(Single.just(response));

        newsRepo.fetch("in")
            .test()
            .assertValueAt(0, resource -> resource.status == Resource.Status.LOADING)
            .assertValueAt(1, resource -> resource.status == Resource.Status.SUCCESS)
            .assertComplete();
        verify(mockNewsDao, times(2)).all();
        verify(mockNewsApi, times(1)).fetch(anyString());
        verify(mockNewsDao, times(1)).addAll(any());
    }

    @Test
    public void assertUnSuccessfulNetworkFetch() {

        when(mockNewsDao.all()).thenReturn(Maybe.just(mockLocalResponse));
        when(mockNewsApi.fetch(anyString())).thenReturn(Single.error(new UnknownHostException("Unable to reach network")));

        newsRepo.fetch("in")
            .test()
            .assertValueAt(0, resource -> resource.status == Resource.Status.LOADING)
            .assertValueAt(1, resource -> resource.status == Resource.Status.ERROR && resource.error instanceof NetworkError)
            .assertComplete();
        verify(mockNewsDao, times(1)).all();
        verify(mockNewsApi, times(1)).fetch(anyString());
        verify(mockNewsDao, times(0)).addAll(any());
    }
}
