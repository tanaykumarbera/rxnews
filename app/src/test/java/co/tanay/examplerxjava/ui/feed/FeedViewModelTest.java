package co.tanay.examplerxjava.ui.feed;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import co.tanay.examplerxjava.TestSchedulersImpl;
import co.tanay.examplerxjava.data.news.NewsRepo;
import co.tanay.examplerxjava.rx.AppSchedulers;
import co.tanay.examplerxjava.utils.Resource;
import io.reactivex.Observable;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class FeedViewModelTest {

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    NewsRepo newsRepo;

    AppSchedulers appSchedulers = new TestSchedulersImpl();

    FeedFragmentViewModel viewModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        viewModel = new FeedFragmentViewModel(appSchedulers, newsRepo);
    }

    @Test
    public void assertFetch() {
        when(newsRepo.fetch(anyString())).thenReturn(Observable.just(Resource.loading(null)));
        viewModel.refresh().observeForever(res -> {
            verify(newsRepo, times(1)).fetch(anyString());
            assert res.status == Resource.Status.LOADING;
        });
    }
}
