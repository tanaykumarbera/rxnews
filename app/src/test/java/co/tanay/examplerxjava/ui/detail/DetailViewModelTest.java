package co.tanay.examplerxjava.ui.detail;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import co.tanay.examplerxjava.TestSchedulersImpl;
import co.tanay.examplerxjava.data.news.NewsRepo;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import co.tanay.examplerxjava.rx.AppSchedulers;
import co.tanay.examplerxjava.utils.Resource;
import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class DetailViewModelTest {

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    NewsRepo newsRepo;

    @Mock
    Resource<NewsArticle> mockResource;


    AppSchedulers appSchedulers = new TestSchedulersImpl();

    DetailFragmentViewModel viewModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        viewModel = new DetailFragmentViewModel(appSchedulers, newsRepo);
    }

    @Test
    public void assertFetch() {

        when(newsRepo.get(anyString())).thenReturn(Single.just(mockResource));

        viewModel.load("someurl").observeForever(res -> {
            verify(newsRepo, times(1)).get(anyString());
            assert res == mockResource;
        });
    }
}
