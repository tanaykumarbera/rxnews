package co.tanay.examplerxjava.data.news.net.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Response structure we receive from server
 */
public class NewsResponse {

    @SerializedName("status")
    public String status;

    @SerializedName("totalResults")
    public int totalResults;

    @SerializedName("articles")
    public List<NewsResponseItem> articles;
}
