package co.tanay.examplerxjava.data.news;

import java.util.List;

import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import co.tanay.examplerxjava.utils.Resource;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface NewsRepo {

    Observable<Resource<List<NewsArticle>>> fetch(String country);

    Single<Resource<NewsArticle>> get(String url);
}
