package co.tanay.examplerxjava.data.news.local.models;

import org.threeten.bp.Instant;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import co.tanay.examplerxjava.db.Converters;

@Entity(tableName = "news_article")
@TypeConverters(Converters.class)
public class NewsArticle {

    @ColumnInfo(name = "title")
    public String title;

    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "image_url")
    public String imageUrl;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "content_url")
    public String contentUrl;

    @ColumnInfo(name = "published_at")
    public Instant publishedAt;

    @ColumnInfo(name = "published_by")
    public String publishedBy;
}
