package co.tanay.examplerxjava.data.news;

import org.threeten.bp.Instant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import co.tanay.examplerxjava.data.news.local.NewsDao;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import co.tanay.examplerxjava.data.news.net.NewsApi;
import co.tanay.examplerxjava.data.news.net.model.NewsResponseItem;
import co.tanay.examplerxjava.utils.CacheableResourceLoader;
import co.tanay.examplerxjava.utils.Resource;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class NewsRepoImpl implements NewsRepo {

    private NewsApi newsApi;
    private NewsDao newsDao;

    @Inject
    public NewsRepoImpl(NewsApi newsApi, NewsDao newsDao) {
        this.newsApi = newsApi;
        this.newsDao = newsDao;
    }

    @Override
    public Observable<Resource<List<NewsArticle>>> fetch(String country) {
        return new NewsLoader(country).get();
    }

    @Override
    public Single<Resource<NewsArticle>> get(String url) {
        return newsDao.get(url)
            .toSingle()
            .map(Resource::success)
            .onErrorReturn(Resource::error);
    }

    protected class NewsLoader extends CacheableResourceLoader<List<NewsArticle>, List<NewsResponseItem>> {

        private String country;

        NewsLoader(String country) {
            this.country = country;
        }

        @Override
        protected Single<List<NewsArticle>> getLocal() {
            return newsDao.all()
                .toSingle()
                .onErrorReturn(err -> Collections.emptyList());
        }

        @Override
        protected Single<List<NewsResponseItem>> getRemote() {
            return newsApi.fetch(country)
                .map(newsResponse -> newsResponse.articles);
        }

        @Override
        protected Completable saveCallResult(List<NewsArticle> data) {
            return newsDao.addAll(data);
        }

        @Override
        protected List<NewsArticle> mapper(List<NewsResponseItem> data) {
            List<NewsArticle> articles = new ArrayList<>();
            if (data != null && !data.isEmpty()) {
                for (NewsResponseItem item : data) {
                    try {
                        articles.add(from(item));
                    } catch (Throwable e) {
                        // error while mapping
                        e.printStackTrace();
                    }
                }
            }
            return articles;
        }

        private NewsArticle from(NewsResponseItem item) {
            NewsArticle article = new NewsArticle();
            article.title = item.title;
            article.contentUrl = item.url;
            article.imageUrl = item.urlToImage;
            article.publishedBy = item.author;
            article.description = item.description;
            article.publishedAt = Instant.ofEpochMilli(item.publishedAt.getTime());
            return article;
        }
    }
}
