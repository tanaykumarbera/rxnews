package co.tanay.examplerxjava.data.news.local;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import io.reactivex.Completable;
import io.reactivex.Maybe;

@Dao
public interface NewsDao {

    @Query("SELECT * FROM news_article ORDER BY published_at DESC")
    Maybe<List<NewsArticle>> all();

    @Query("SELECT * FROM news_article WHERE content_url = :url LIMIT 1")
    Maybe<NewsArticle> get(String url);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable addAll(List<NewsArticle> newsArticles);

    @Query("DELETE FROM news_article")
    Completable clear();
}
