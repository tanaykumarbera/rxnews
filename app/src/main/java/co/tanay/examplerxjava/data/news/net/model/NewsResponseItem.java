package co.tanay.examplerxjava.data.news.net.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Item structure for article entities we receive from server
 */
public class NewsResponseItem {

    public class Source {

        @SerializedName("id")
        public String id;

        @SerializedName("name")
        public String name;
    }

    @SerializedName("source")
    public Source source;

    @SerializedName("author")
    public String author;

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("url")
    public String url;

    @SerializedName("urlToImage")
    public String urlToImage;

    @SerializedName("publishedAt")
    public Date publishedAt;

    @SerializedName("content")
    public String content;
}
