package co.tanay.examplerxjava.data.news.net;

import co.tanay.examplerxjava.data.news.net.model.NewsResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApi {

    @GET("top-headlines")
    Single<NewsResponse> fetch(@Query("country") String country);
}
