package co.tanay.examplerxjava.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * A custom scope to mark availability to
 * application wide scope
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppScope {
}
