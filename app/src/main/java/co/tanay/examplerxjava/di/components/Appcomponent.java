package co.tanay.examplerxjava.di.components;

import co.tanay.examplerxjava.di.modules.AppModule;
import co.tanay.examplerxjava.di.modules.DataModule;
import co.tanay.examplerxjava.di.modules.FragmentModule;
import co.tanay.examplerxjava.di.modules.NetModule;
import co.tanay.examplerxjava.di.scopes.AppScope;
import dagger.Component;

@AppScope
@Component(modules = {
    AppModule.class,
    NetModule.class,
    DataModule.class,
    FragmentModule.class
})
public interface Appcomponent {

    FragmentComponent.Builder fragmentComponentBuilder();
}
