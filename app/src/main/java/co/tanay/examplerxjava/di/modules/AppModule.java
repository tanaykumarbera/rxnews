package co.tanay.examplerxjava.di.modules;

import android.app.Application;
import android.content.Context;

import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Locale;

import javax.inject.Named;

import co.tanay.examplerxjava.BuildConfig;
import co.tanay.examplerxjava.di.components.FragmentComponent;
import co.tanay.examplerxjava.di.qualifier.ApplicationContext;
import co.tanay.examplerxjava.di.scopes.AppScope;
import co.tanay.examplerxjava.rx.AppSchedulers;
import co.tanay.examplerxjava.rx.AppSchedulersImpl;
import dagger.Module;
import dagger.Provides;

@Module(subcomponents = FragmentComponent.class)
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @AppScope
    @ApplicationContext
    Context appContext() {
        return application;
    }

    @Provides
    @AppScope
    AppSchedulers appSchedulers() {
        return new AppSchedulersImpl();
    }

    @Provides
    @AppScope
    PrettyTime prettyTime() {
        return new PrettyTime(Locale.ENGLISH);
    }

    @Provides
    @AppScope
    Picasso picasso(@ApplicationContext Context context, @Named("PicassoDownloader") OkHttp3Downloader cacheEnabledDownloader) {
        return new Picasso.Builder(context)
            .loggingEnabled(BuildConfig.DEBUG)
            .downloader(cacheEnabledDownloader)
            .build();
    }
}
