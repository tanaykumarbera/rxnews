package co.tanay.examplerxjava.di.modules;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.OkHttp3Downloader;

import java.io.File;
import java.text.DateFormat;

import javax.inject.Named;

import co.tanay.examplerxjava.BuildConfig;
import co.tanay.examplerxjava.data.news.net.NewsApi;
import co.tanay.examplerxjava.di.qualifier.ApplicationContext;
import co.tanay.examplerxjava.di.scopes.AppScope;
import co.tanay.examplerxjava.utils.AuthInterceptor;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    @Provides
    @AppScope
    Gson gson() {
        return new GsonBuilder()
            .setDateFormat(DateFormat.FULL, DateFormat.FULL) // Todo: Revisit while wiring up api
            .create();
    }

    @AppScope
    @Provides
    HttpLoggingInterceptor logInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

    @Provides
    @AppScope
    OkHttpClient okHttpClient(HttpLoggingInterceptor loggingInterceptor) {

        return new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .retryOnConnectionFailure(false)
            .addInterceptor(new AuthInterceptor())
            .build();
    }

    @Provides
    @AppScope
    Retrofit retrofit(OkHttpClient client, Gson gson) {

        return new Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build();
    }

    @Provides
    NewsApi newsApi(Retrofit retrofit) {
        return retrofit.create(NewsApi.class);
    }

    @Provides
    @AppScope
    @Named("PicassoCache")
    Cache cache(@ApplicationContext Context context) {
        File httpCacheDirectory = new File(context.getCacheDir(), "picasso-cache");
        return new Cache(httpCacheDirectory, 50 * 1024 * 1024); // max 50Mb
    }

    @Provides
    @AppScope
    @Named("PicassoOkHttpClient")
    OkHttpClient picassoOkHttpClient(@Named("PicassoCache") Cache cache) {

        return new OkHttpClient.Builder()
            .cache(cache)
            .retryOnConnectionFailure(true)
            .build();
    }

    @Provides
    @AppScope
    @Named("PicassoDownloader")
    OkHttp3Downloader picassoOkHttp3Downloader(@Named("PicassoOkHttpClient") OkHttpClient client) {
        return new OkHttp3Downloader(client);
    }
}
