package co.tanay.examplerxjava.di.modules;

import android.content.Context;

import androidx.room.Room;
import co.tanay.examplerxjava.data.news.NewsRepo;
import co.tanay.examplerxjava.data.news.NewsRepoImpl;
import co.tanay.examplerxjava.data.news.local.NewsDao;
import co.tanay.examplerxjava.data.news.net.NewsApi;
import co.tanay.examplerxjava.db.NewsDatabase;
import co.tanay.examplerxjava.di.qualifier.ApplicationContext;
import co.tanay.examplerxjava.di.scopes.AppScope;
import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Provides
    @AppScope
    NewsDatabase database(@ApplicationContext Context context) {
        return Room.databaseBuilder(context, NewsDatabase.class, "news-ex")
            .fallbackToDestructiveMigration() // Todo: Decide
            .build();
    }

    @Provides
    NewsDao newsDao(NewsDatabase database) {
        return database.newsDao();
    }

    @Provides
    NewsRepo newsRepo(NewsApi newsApi, NewsDao newsDao) {
        return new NewsRepoImpl(newsApi, newsDao);
    }
}
