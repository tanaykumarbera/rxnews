package co.tanay.examplerxjava.di.components;

import androidx.fragment.app.Fragment;
import co.tanay.examplerxjava.di.modules.ViewModelModule;
import co.tanay.examplerxjava.di.scopes.FragmentScope;
import co.tanay.examplerxjava.ui.detail.DetailFragment;
import co.tanay.examplerxjava.ui.feed.FeedFragment;
import dagger.BindsInstance;
import dagger.Subcomponent;

@FragmentScope
@Subcomponent(modules = {
    ViewModelModule.class
})
public interface FragmentComponent {

    void inject(FeedFragment fragment);

    void inject(DetailFragment fragment);

    @Subcomponent.Builder
    interface Builder {

        /**
         * Bind this to the instance.
         * Make fragment available, whenever needed.
         * <p>
         * (We'll need one while initializing viewModels)
         *
         * @param fragment - A fragment to init
         */
        @BindsInstance
        Builder withFragment(Fragment fragment);

        FragmentComponent build();
    }
}
