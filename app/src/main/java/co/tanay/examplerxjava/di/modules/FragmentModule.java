package co.tanay.examplerxjava.di.modules;

import androidx.lifecycle.ViewModelProvider;
import co.tanay.examplerxjava.di.components.FragmentComponent;
import co.tanay.examplerxjava.di.scopes.AppScope;
import co.tanay.examplerxjava.ui.BaseViewModelFactory;
import dagger.Binds;
import dagger.Module;

/**
 * A custom module to provide dependencies at fragment level.
 * Also brings in sub component `FragmentComponent` into the graph
 */
@Module(subcomponents = FragmentComponent.class)
public interface FragmentModule {

    @Binds
    @AppScope
    ViewModelProvider.Factory bindViewModelFactory(BaseViewModelFactory viewModelFactory);
}
