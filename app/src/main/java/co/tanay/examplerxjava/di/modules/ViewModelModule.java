package co.tanay.examplerxjava.di.modules;

import androidx.lifecycle.ViewModel;
import co.tanay.examplerxjava.di.keys.ViewModelKey;
import co.tanay.examplerxjava.di.scopes.FragmentScope;
import co.tanay.examplerxjava.ui.detail.DetailFragmentViewModel;
import co.tanay.examplerxjava.ui.feed.FeedFragmentViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @FragmentScope
    @ViewModelKey(FeedFragmentViewModel.class)
    public abstract ViewModel bindFeedViewModel(FeedFragmentViewModel viewModel);

    @Binds
    @IntoMap
    @FragmentScope
    @ViewModelKey(DetailFragmentViewModel.class)
    public abstract ViewModel bindDetailViewModel(DetailFragmentViewModel viewModel);
}
