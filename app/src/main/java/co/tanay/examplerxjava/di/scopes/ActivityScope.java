package co.tanay.examplerxjava.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Custom scope to mark availability within a
 * running activity
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
@interface ActivityScope {
}
