package co.tanay.examplerxjava.utils;

public class Resource<Type> {

    public enum Status {
        SUCCESS, LOADING, ERROR
    }

    public Status status;
    public Type data;
    public Throwable error;

    private Resource(Status status) {
        this.status = status;
    }

    public static <Type> Resource<Type> success(Type data) {
        Resource resource = new Resource<Type>(Status.SUCCESS);
        resource.data = data;
        return resource;
    }

    public static <Type> Resource<Type> error(Throwable err) {
        Resource resource = new Resource<Type>(Status.ERROR);
        resource.error = err;
        return resource;
    }

    public static <Type> Resource<Type> loading(Type data) {
        Resource resource = new Resource<Type>(Status.LOADING);
        resource.data = data;
        return resource;
    }
}
