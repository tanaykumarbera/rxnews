package co.tanay.examplerxjava.utils;

/**
 * Denotes err out due to network fault
 */
public class NetworkError extends Throwable {

    public NetworkError(Throwable cause) {
        super(cause);
    }
}
