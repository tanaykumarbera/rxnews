package co.tanay.examplerxjava.utils;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * A utility class to cache loaded results in local
 * database. Load from cache to show initial loading state, while continue fetch
 * from network. On network result, update local and re fetch from local to maintain
 * single truth principle
 * <p>
 * ** Same as of NetworkBoundResource
 *
 * @param <LocalType>  - Type of data being stored locally
 * @param <RemoteType> - Type of entities coming down from server.
 */
public abstract class CacheableResourceLoader<LocalType, RemoteType> {

    protected abstract Single<RemoteType> getRemote();

    protected abstract Single<LocalType> getLocal();

    protected abstract Completable saveCallResult(LocalType data);

    protected abstract LocalType mapper(RemoteType data);

    public Observable<Resource<LocalType>> get() {

        return getLocal()
            .onErrorResumeNext(err -> Single.error(new DatabaseError(err)))
            .map(Resource::loading)
            .toObservable()
            .mergeWith(getRemote()
                .onErrorResumeNext(err -> Single.error(new NetworkError(err)))
                .map(this::mapper)
                .flatMap(networkResource -> saveCallResult(networkResource).toSingleDefault(true))
                .flatMap(success -> getLocal().onErrorResumeNext(err -> Single.error(new DatabaseError(err))))
                .map(Resource::success)
                .onErrorReturn(Resource::error))
            .onErrorReturn(Resource::error);
    }
}
