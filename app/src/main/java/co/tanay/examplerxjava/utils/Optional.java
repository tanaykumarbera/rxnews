package co.tanay.examplerxjava.utils;

/**
 * A minimal impl of Optional
 *
 * @param <Type> The type of data being dealt with
 */
public class Optional<Type> {

    private Type content;

    private Optional(Type data) {
        content = data;
    }

    public boolean isPresent() {
        return content != null;
    }

    public Type get() {
        return content;
    }

    public Type getOr(Type defaultValue) {
        return content == null ? defaultValue : content;
    }

    public static <Type> Optional<Type> of(Type data) {
        return new Optional<>(data);
    }

    public static <Type> Optional<Type> empty() {
        return new Optional<>(null);
    }
}
