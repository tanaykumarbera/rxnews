package co.tanay.examplerxjava.utils;

/**
 * Any error due to database
 */
public class DatabaseError extends Throwable {

    public DatabaseError(Throwable cause) {
        super(cause);
    }
}
