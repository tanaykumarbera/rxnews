package co.tanay.examplerxjava.rx;

import org.reactivestreams.Publisher;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;

public abstract class AppSchedulers {

    public abstract Scheduler io();

    public abstract Scheduler ui();

    public <R> ObservableSource<R> apply(Observable<R> resourceObservable) {
        return resourceObservable.subscribeOn(io()).observeOn(ui());
    }

    public <R> Publisher<R> apply(Flowable<R> resourceFlowable) {
        return resourceFlowable.subscribeOn(io()).observeOn(ui());
    }
}
