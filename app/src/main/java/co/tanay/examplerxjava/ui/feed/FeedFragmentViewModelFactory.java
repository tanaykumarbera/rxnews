package co.tanay.examplerxjava.ui.feed;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import co.tanay.examplerxjava.data.news.NewsRepo;
import co.tanay.examplerxjava.rx.AppSchedulers;

public class FeedFragmentViewModelFactory implements ViewModelProvider.Factory {

    private AppSchedulers schedulers;
    private NewsRepo newsRepo;

    @Inject
    public FeedFragmentViewModelFactory(AppSchedulers schedulers, NewsRepo newsRepo) {
        this.schedulers = schedulers;
        this.newsRepo = newsRepo;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new FeedFragmentViewModel(schedulers, newsRepo);
    }
}
