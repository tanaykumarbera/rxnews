package co.tanay.examplerxjava.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import co.tanay.examplerxjava.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
