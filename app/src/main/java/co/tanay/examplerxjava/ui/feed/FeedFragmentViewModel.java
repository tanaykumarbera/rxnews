package co.tanay.examplerxjava.ui.feed;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import co.tanay.examplerxjava.data.news.NewsRepo;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import co.tanay.examplerxjava.rx.AppSchedulers;
import co.tanay.examplerxjava.ui.BaseViewModel;
import co.tanay.examplerxjava.utils.Resource;
import io.reactivex.BackpressureStrategy;

public class FeedFragmentViewModel extends BaseViewModel {

    private AppSchedulers schedulers;
    private NewsRepo newsRepo;

    @Inject
    FeedFragmentViewModel(AppSchedulers schedulers, NewsRepo newsRepo) {
        this.schedulers = schedulers;
        this.newsRepo = newsRepo;
    }

    /**
     * Init a refresh action, which is going to fetch list of
     * articles from {@link co.tanay.examplerxjava.data.news.NewsRepo}
     *
     * @return A livedata which can be observed to listen
     * for updated news feed.
     */
    LiveData<Resource<List<NewsArticle>>> refresh() {
        return LiveDataReactiveStreams.fromPublisher(newsRepo.fetch("in")
            // let underlying LiveData subscribe,
            // or else initial event is skipped.
            // Todo: find a better way
            .delay(1, TimeUnit.SECONDS)
            .toFlowable(BackpressureStrategy.MISSING)
            .compose(schedulers::apply));
    }
}
