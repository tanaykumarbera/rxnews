package co.tanay.examplerxjava.ui.feed;

import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import co.tanay.examplerxjava.R;
import co.tanay.examplerxjava.databinding.FragmentFeedBinding;
import co.tanay.examplerxjava.di.components.Appcomponent;
import co.tanay.examplerxjava.ui.BaseFragment;
import co.tanay.examplerxjava.ui.BaseViewModelFactory;
import co.tanay.examplerxjava.ui.feed.adapter.FeedListAdapter;
import co.tanay.examplerxjava.utils.Resource;

public class FeedFragment extends BaseFragment<FragmentFeedBinding> {

    @Inject
    BaseViewModelFactory factory;

    @Inject
    FeedListAdapter adapter;

    private FeedFragmentViewModel feedFragmentViewModel;

    @Override
    protected void inject(Appcomponent appcomponent) {
        appcomponent.fragmentComponentBuilder()
            .withFragment(this)
            .build()
            .inject(this);
        feedFragmentViewModel = ViewModelProviders.of(this, factory).get(FeedFragmentViewModel.class);
    }

    @Override
    protected int layoutRes() {
        return R.layout.fragment_feed;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.feedList.setAdapter(adapter);
        binding.refreshLayout.setOnRefreshListener(this::refresh);
        adapter.attachListener(article -> Navigation
            .findNavController(view)
            .navigate(FeedFragmentDirections.actionFeedFragmentToDetailFragment(article.contentUrl)));
        refresh();
    }

    private void refresh() {
        binding.refreshLayout.setRefreshing(true);
        feedFragmentViewModel.refresh().observe(this, newsResource -> {
            binding.refreshLayout.setRefreshing(false);
            if (newsResource.status != Resource.Status.ERROR) {
                adapter.update(newsResource.data);
                if (newsResource.data != null && !newsResource.data.isEmpty()) {
                    binding.placeholder.setVisibility(View.GONE);
                    binding.feedList.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
