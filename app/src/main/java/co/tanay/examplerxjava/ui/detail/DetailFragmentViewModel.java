package co.tanay.examplerxjava.ui.detail;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import co.tanay.examplerxjava.data.news.NewsRepo;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import co.tanay.examplerxjava.rx.AppSchedulers;
import co.tanay.examplerxjava.ui.BaseViewModel;
import co.tanay.examplerxjava.utils.Resource;

public class DetailFragmentViewModel extends BaseViewModel {

    private AppSchedulers schedulers;
    private NewsRepo newsRepo;

    @Inject
    DetailFragmentViewModel(AppSchedulers schedulers, NewsRepo newsRepo) {
        this.schedulers = schedulers;
        this.newsRepo = newsRepo;
    }

    /**
     * Init a load action, which is going to fetch the
     * articles from {@link NewsRepo}
     *
     * @return A livedata which can be observed to listen
     * for updated news resource.
     */
    LiveData<Resource<NewsArticle>> load(String url) {
        return LiveDataReactiveStreams.fromPublisher(newsRepo.get(url)
            .toFlowable()
            .compose(schedulers::apply));
    }
}
