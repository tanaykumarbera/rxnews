package co.tanay.examplerxjava.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import co.tanay.examplerxjava.App;
import co.tanay.examplerxjava.di.components.Appcomponent;

public abstract class BaseFragment<Binding extends ViewDataBinding> extends Fragment {

    protected Binding binding;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        inject(((App) context.getApplicationContext()).appComponent());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, layoutRes(), container, false);
        return binding.getRoot();
    }

    abstract protected void inject(Appcomponent appcomponent);

    @LayoutRes
    abstract protected int layoutRes();
}
