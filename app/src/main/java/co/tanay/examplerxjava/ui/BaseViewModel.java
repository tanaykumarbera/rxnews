package co.tanay.examplerxjava.ui;

import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BaseViewModel extends ViewModel {

    private CompositeDisposable mDisposable = new CompositeDisposable();

    protected void autoDispose(Disposable disposable) {
        mDisposable.add(disposable);
    }

    @Override
    protected void onCleared() {
        mDisposable.dispose();
        super.onCleared();
    }
}
