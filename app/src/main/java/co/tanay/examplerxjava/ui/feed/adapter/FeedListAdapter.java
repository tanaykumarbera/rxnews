package co.tanay.examplerxjava.ui.feed.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import co.tanay.examplerxjava.databinding.FeedLargeItemBinding;
import co.tanay.examplerxjava.databinding.FeedListItemBinding;

public class FeedListAdapter extends RecyclerView.Adapter<FeedListAdapter.VH> {

    enum ViewType {
        LARGE_ITEM,
        LIST_ITEM
    }

    private Callback callback;
    private Picasso picasso;
    private PrettyTime prettyTime;
    private List<NewsArticle> articles = new ArrayList<>();

    @Inject
    public FeedListAdapter(Picasso picasso, PrettyTime prettyTime) {
        this.picasso = picasso;
        this.prettyTime = prettyTime;
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (ViewType.values()[viewType]) {
            case LARGE_ITEM:
                return new VHLarge(FeedLargeItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
            case LIST_ITEM:
            default:
                return new VHList(FeedListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? ViewType.LARGE_ITEM.ordinal() : ViewType.LIST_ITEM.ordinal();
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.bind(articles.get(position));
    }

    public void update(List<NewsArticle> news) {

        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffCheckCallBack(articles, news), false);

        articles.clear();
        articles.addAll(news);

        diffResult.dispatchUpdatesTo(this);
    }

    public void attachListener(Callback callback) {
        this.callback = callback;
    }

    abstract class VH<T extends ViewDataBinding> extends RecyclerView.ViewHolder {

        T binding;

        VH(T binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        abstract void bind(NewsArticle article);
    }

    class VHList extends VH<FeedListItemBinding> {

        VHList(FeedListItemBinding binding) {
            super(binding);
        }

        @Override
        void bind(NewsArticle article) {
            binding.setNHeadline(article.title);
            binding.setNTime(prettyTime.format(new Date(article.publishedAt.toEpochMilli())));
            binding.setOnClick(v -> {
                if (callback != null) callback.onSelected(article);
            });
            picasso.load(article.imageUrl).into(binding.image);
        }
    }

    class VHLarge extends VH<FeedLargeItemBinding> {

        VHLarge(FeedLargeItemBinding binding) {
            super(binding);
        }

        @Override
        void bind(NewsArticle article) {
            binding.setNHeadline(article.title);
            binding.setOnClick(v -> {
                if (callback != null) callback.onSelected(article);
            });
            picasso.load(article.imageUrl).into(binding.image);
        }
    }

    public interface Callback {
        void onSelected(NewsArticle article);
    }

    private class DiffCheckCallBack extends DiffUtil.Callback {

        private List<NewsArticle> oldListRef, newListRef;

        DiffCheckCallBack(List<NewsArticle> oldListRef, List<NewsArticle> newListRef) {
            this.oldListRef = oldListRef;
            this.newListRef = newListRef;
        }

        @Override
        public int getOldListSize() {
            return oldListRef.size();
        }

        @Override
        public int getNewListSize() {
            return newListRef == null ? 0 : newListRef.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldListRef.get(oldItemPosition).contentUrl.equals(newListRef.get(newItemPosition).contentUrl);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return true; // very less likely to change
        }
    }
}
