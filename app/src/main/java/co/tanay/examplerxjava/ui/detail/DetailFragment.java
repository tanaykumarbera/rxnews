package co.tanay.examplerxjava.ui.detail;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import co.tanay.examplerxjava.R;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;
import co.tanay.examplerxjava.databinding.FragmentDetailBinding;
import co.tanay.examplerxjava.di.components.Appcomponent;
import co.tanay.examplerxjava.ui.BaseFragment;
import co.tanay.examplerxjava.ui.BaseViewModelFactory;
import co.tanay.examplerxjava.utils.Resource;

public class DetailFragment extends BaseFragment<FragmentDetailBinding> {

    @Inject
    Picasso picasso;

    @Inject
    PrettyTime prettyTime;

    @Inject
    BaseViewModelFactory factory;

    private DetailFragmentViewModel detailFragmentViewModel;

    @Override
    protected void inject(Appcomponent appcomponent) {
        appcomponent.fragmentComponentBuilder()
            .withFragment(this)
            .build()
            .inject(this);
        detailFragmentViewModel = ViewModelProviders.of(this, factory).get(DetailFragmentViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setOnBack(v -> Navigation.findNavController(v).popBackStack());
        binding.webview.getSettings().setJavaScriptEnabled(true);
        binding.webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                binding.webview.setVisibility(View.GONE);
                binding.meta.setVisibility(View.VISIBLE);
                Snackbar
                    .make(binding.getRoot(), getString(R.string.no_internet_webview_error), Snackbar.LENGTH_LONG)
                    .show();
            }
        });
        detailFragmentViewModel.load(DetailFragmentArgs.fromBundle(getArguments()).getUrl())
            .observe(this, newsArticleResource -> {
                if (newsArticleResource.status == Resource.Status.SUCCESS) {
                    NewsArticle article = newsArticleResource.data;
                    binding.setNHeadline(article.title);
                    binding.setNContent(article.description);
                    binding.setNWhen(prettyTime.format(new Date(article.publishedAt.toEpochMilli())));
                    binding.setNOnContinue(v -> {
                        binding.meta.setVisibility(View.GONE);
                        binding.webview.setVisibility(View.VISIBLE);
                        binding.webview.loadUrl(article.contentUrl);
                    });
                    picasso.load(article.imageUrl).into(binding.image);
                } else {
                    // show error?
                }
            });
    }

    @Override
    protected int layoutRes() {
        return R.layout.fragment_detail;
    }
}
