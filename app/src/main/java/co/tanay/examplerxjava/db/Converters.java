package co.tanay.examplerxjava.db;

import org.threeten.bp.Instant;

import androidx.room.TypeConverter;

public class Converters {

    @TypeConverter
    public static Instant fromTimeStamp(long timeStamp) {
        return Instant.ofEpochMilli(timeStamp);
    }

    @TypeConverter
    public static long toTimeStamp(Instant instant) {
        return instant.toEpochMilli();
    }
}
