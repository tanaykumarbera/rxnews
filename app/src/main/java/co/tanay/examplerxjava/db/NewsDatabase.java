package co.tanay.examplerxjava.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import co.tanay.examplerxjava.data.news.local.NewsDao;
import co.tanay.examplerxjava.data.news.local.models.NewsArticle;

@Database(entities = NewsArticle.class, exportSchema = false, version = 1)
public abstract class NewsDatabase extends RoomDatabase {

    public abstract NewsDao newsDao();
}
