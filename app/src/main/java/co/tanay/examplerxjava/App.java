package co.tanay.examplerxjava;

import android.app.Application;

import co.tanay.examplerxjava.di.components.Appcomponent;
import co.tanay.examplerxjava.di.components.DaggerAppcomponent;
import co.tanay.examplerxjava.di.modules.AppModule;

public class App extends Application {

    private Appcomponent appcomponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appcomponent = DaggerAppcomponent
            .builder()
            .appModule(new AppModule(this))
            .build();
    }

    public Appcomponent appComponent() {
        return appcomponent;
    }
}
